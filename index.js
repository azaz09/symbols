//Mała klasa symboli by AJ

const canvasElem = document.getElementById('canvas');
var ctx = canvasElem.getContext('2d'); // globalna zmienna 
//let  - zmienna dla danej funkcji / zmienna o zasięgu blokowym
//const - stała wartość
ctx.textBaseline="middle";
ctx.textAlign="Center";
ctx.font="32px Times New Roman";
class Symbols {
    
    rectangle(x, y, width, height) {

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x + width, y);
        ctx.lineTo(x + width, y + height);
        ctx.lineTo(x, y + height);
        ctx.lineTo(x, y);
        ctx.stroke();

    }

    lozenge(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x - width, y + height);
        ctx.lineTo(x + width, y + height);
        ctx.lineTo(x + 2 * width, y);
        ctx.lineTo(x, y);
        ctx.stroke();
    }

    deltoid(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x - width, y + height);
        ctx.lineTo(x, y + 2 * height);
        ctx.lineTo(x + width, y + height);
        ctx.lineTo(x, y);
        ctx.stroke();

    }

    hexagon(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x - width, y + height);
        ctx.lineTo(x, y + 2 * height);
        ctx.lineTo(x + width, y + 2 * height);
        ctx.lineTo(x + 2 * width, y + height);
        ctx.lineTo(x + width, y);
        ctx.lineTo(x, y);
        ctx.stroke();
    }
    roadArrow(x, y, length, aWidth, aHeight, option) {
        option=option.toLowerCase();
        if (option == "d") {
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x, y + length);
            ctx.stroke();
            ctx.moveTo(x,(length/2+y) + aHeight);
            ctx.lineTo(x-aWidth,(length/2+y)-aHeight);
            ctx.lineTo(x+aWidth, (length/2+y)-aHeight);
            ctx.lineTo(x, (length/2+y) + aHeight);
            ctx.fill();
        }

        if (option == "p") {
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x + length, y);
            ctx.stroke();
            ctx.moveTo((length / 2 + x) + aWidth, y);
            ctx.lineTo((length / 2 + x) - aWidth, y - aHeight);
            ctx.lineTo((length / 2 + x) - aWidth, y + aHeight);
            ctx.lineTo((length / 2 + x) + aWidth, y);
            ctx.fill();
        }

    }

    cross(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x + width, y);
        ctx.moveTo(width / 2, y - height / 2);
        ctx.lineTo(x + width / 2, y + height / 2);
        ctx.stroke();
    }

    merger(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x + width, y);
        ctx.moveTo(x + width, y - height / 2);
        ctx.lineTo(x + width, y + height / 2);
        ctx.stroke();
    }

    circle(x, y, r) {
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 10 * Math.PI);
        ctx.stroke();
    }

    corner(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x, y + height);
        ctx.lineTo(x + width, y + 2 * height);
        ctx.lineTo(x + 2 * width, y + height);
        ctx.lineTo(x + 2 * width, y);
        ctx.lineTo(x, y);
        ctx.stroke();
    }

    rectangleCurve(x, y, width, height, radius) {

        ctx.beginPath();
        ctx.moveTo(x, y + radius);
        ctx.lineTo(x, y + height - radius);
        ctx.quadraticCurveTo(x, y + height, x + radius, y + height);
        ctx.lineTo(x + width - radius, y + height);
        ctx.quadraticCurveTo(x + width, y + height, x + width, y + height - radius);
        ctx.lineTo(x + width, y + radius);
        ctx.quadraticCurveTo(x + width, y, x + width - radius, y);
        ctx.lineTo(x + radius, y);
        ctx.quadraticCurveTo(x, y, x, y + radius);
        ctx.stroke();
    }

    parallelLine(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x + width, y);
        ctx.moveTo(x + width, y + height);
        ctx.lineTo(x, y + height);
        ctx.stroke();
    }

    commentFigure(x, y, width, height) {
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x + width, y);
        ctx.lineTo(x + width, y + height);
        ctx.lineTo(x, y + height);
        ctx.moveTo(x + width, y + height / 2);
        ctx.lineTo(x + 2 * width, y + height / 2);
        ctx.stroke();
    }
}

function rysuj() {
    var figure1 = new Symbols();
    figure1.rectangle(0, 0, 120, 60);
    figure1.lozenge(40, 120, 40, 60);
    figure1.deltoid(60, 220, 60, 30);
    figure1.hexagon(40, 320, 40, 30);
    figure1.roadArrow(0, 440, 120, 6, 6, "p");
    figure1.cross(0, 560, 120, 120);
    figure1.merger(0, 650, 120, 40);
    figure1.circle(60, 740, 40);
    figure1.corner(0, 820, 30, 50);
    figure1.rectangleCurve(0, 960, 120, 60, 20);
    figure1.parallelLine(0, 1060, 120, 30);
    figure1.commentFigure(0, 1130, 30, 60);
}


function algorytm() {
    var figure = new Symbols();
    figure.lozenge(450, 50, 80, 60);
    figure.roadArrow(450, 110, 40, 6, 6, "d");
    figure.lozenge(450, 150, 80, 60);
    figure.roadArrow(450, 210, 40, 6, 6, "d");
    figure.lozenge(450, 250, 80, 60);
    figure.roadArrow(450, 310, 40, 6, 6, "d");
    figure.rectangle(400,350, 200, 60);
    figure.roadArrow(450, 410, 40, 6, 6, "d");
    figure.rectangle(400,450, 200, 60);
    figure.roadArrow(450, 510, 40, 6, 6, "d");
    figure.rectangle(400,550,200,60);
    figure.roadArrow(450, 610, 40, 6, 6, "d");
    figure.lozenge(400, 650, 240, 70);
    ctx.fillText("var a", 450, 82);
    ctx.fillText("var b", 450, 182);
    ctx.fillText("var c", 450, 282);
    ctx.fillText("var l = [a, b, c]", 405, 382);
    ctx.fillText("l.sort()", 450, 482);
    ctx.fillText("l.reverse()", 450, 582);
    ctx.fillText('alert(l[0]+" "+l[1]+" "+l[2])', 350, 682);
    liczba();
};

